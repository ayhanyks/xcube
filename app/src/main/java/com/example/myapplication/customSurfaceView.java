package com.example.myapplication;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;

import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;


import java.util.ArrayList;

import java.util.Locale;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;






class  point{
    int N;
    long id;
    Float[] px;

    public point(int n){
        N=n;
        px=new Float[N];
    }
}

class edge{
    //VEC p1;
    //VEC p2;
    int pid1;
    int pid2;
}

class VEC{
    public int N;
    public Float[] V;

    public VEC(int dim){
        N=dim;
        V=new Float[N];
        for(int i=0;i<N;i++)
        {
            V[i]= Float.valueOf(0);
        }
    }
}

class SQMatrix{

    public int N;
    Float M[];

    private int getIndex(int x, int y)
    {
        return y*N+x;
    }
    public SQMatrix(int n){
        N=n;
        //create an identity matrix
        M=new Float[N*N];
        for(int x=0;x<N;x++) {
            for (int y = 0; y < N; y++) {
                if(x==y)
                {
                    M[getIndex(x,y)]= Float.valueOf(1);
                }
                else
                {
                    M[getIndex(x,y)]= Float.valueOf(0);
                }
            }
        }
    }


    public Float getVal(int x, int y)
    {
        return M[getIndex(x,y)];
    }

    public void setVal(int x, int y, Float val)
    {
        M[getIndex(x,y)]=val;
    }

    public SQMatrix multiplyWith(SQMatrix M){

        if(N!=M.N)
        {
            return null;
        }

        SQMatrix MM=new SQMatrix(N);

        for (int x=0; x<N;x++){

            for(int y=0;y<N;y++)
            {
                //row (y) from M1, col(x) from M2

                Float T= Float.valueOf(0);
                for(int q=0;q<N;q++)
                {
                    T=T+getVal(q,y)*M.getVal(x,q);
                }
                MM.setVal(x,y,T);
            }
        }
        return MM;
    }

    public VEC multiplyWith(VEC V){
        if(N!=V.N)
        {
            return null;
        }
        VEC VV=new VEC(N);

        for (int v=0; v<N;v++){
            Float T= (float) 0;
            for(int q=0;q<N;q++)
            {
                T=T+ getVal(v, q) * (V.V[q]);
            }
            VV.V[v]=T;
        }
        return VV;
    }

    public void printMatrix(){
        String str="..\n";
        for (int x=0; x<N;x++) {


            for (int y = 0; y < N; y++) {

                str=str+"\t\t"+String.valueOf(getVal(x,y));

            }
            str=str+"\n";

        }
        Log.d("AYH",str);
    }

}

class rotAngle{
    int p1;
    int p2;
    Float angle;
    public rotAngle(){
        p1=0;
        p2=0;
        angle=Float.valueOf(0);
    }
}

class box{
    int NDIM;
    boolean bbusy=false;
    //ArrayList<point>points;
    VEC[] points;

    edge[] edges;
    //ArrayList<edge>edges;
    rotAngle[] rotationAngles;

    SQMatrix rotationMatrix;

    int xoff;
    int yoff;
    int color;
    int selected;
    Double zoomRatio;
    Double scaleRatio;
    public box(int N){
        NDIM=N;
        selected=0;
        edges=new edge[(1<<NDIM)*NDIM/2];
        //points=new ArrayList<point>();
        //edges=new ArrayList<edge>();
        createPoints();
        findEdges();
        createRotationAngles();
        calculateRotationMatrix();
        scaleRatio= 100d;
    }
    public  void reDim(int N){
        bbusy=true;
        NDIM=N;
        edges=new edge[(1<<NDIM)*NDIM/2];
        createPoints();
        findEdges();
        createRotationAngles();
        calculateRotationMatrix();
        bbusy=false;
    }

    public  void zoom(double ratio)
    {
        scaleRatio=scaleRatio*ratio;
    }
    private int bitDiff(long b1,long b2){
        long bxor=(b1^b2);
        int diff=0;
        Log.d("AYH","calc diff"+String.valueOf(b1)+"-"+String.valueOf(b2));
        while(bxor>0)
        {
            if((bxor & 0x1)==1)
            {
                diff++;
            }
            bxor=bxor >>1;
        }
        return diff;
    }
    private  void createPoints(){

        points=new VEC[(1<<NDIM)];

        for (int i=0;i<(1<<NDIM);i++)
        {
            VEC p=new VEC(NDIM);

            for (int j=0;j<NDIM;j++)
            {
                if (( (1<<j) & i)>0){
                    p.V[j]=(float) 1.0;
                }
                else
                {
                    p.V[j]=(float) -1.0;
                }
            }
            points[i]=new VEC(NDIM);
            points[i]=p;

        }
    }

    private int comb2(int N)
    {
        return N*(N-1)/2;
    }

    private void findEdges(){
        int i,j;
        int ex=0;
        Log.d("AYH","findEdges:"+(1<<NDIM));
        for (i=0;i<(1<<NDIM)-1;i=i+1)
        {

            for (j=i+1;j<(1<<NDIM);j++)
            {
                Log.d("AYH","pi="+String.valueOf(i)+" pj="+String.valueOf(j));
                if(bitDiff(i,j)==1)
                {
                    Log.d("AYH","addEdge");
                    edge e=new edge();
                    e.pid1=i;
                    e.pid2=j;
                    edges[ex]=e;
                    ex++;
                    //edges.add(e);
                }
            }

        }
        Log.d("AYH","found edges="+String.valueOf(edges.length));
    }

    private void createRotationAngles()
    {
        rotationAngles=new rotAngle[comb2(NDIM)];

        int rix=0;
        //Log.d("AYH","createRotationAngles"+String.valueOf(rotationAngles));
        for(int x=0;x<NDIM-1;x++)
        {
            for(int y=x+1;y<NDIM;y++)
            {
                rotationAngles[rix]=new rotAngle();
                Log.d("AYH","rotAngle P"+String.valueOf(rix));
                rotationAngles[rix].angle=new Random().nextFloat()/10;
                rotationAngles[rix].p1=x;
                rotationAngles[rix].p2=y;
                Log.d("AYH","p1="+String.valueOf(x)+" p2="+String.valueOf(y)+" a="+String.valueOf(rotationAngles[rix].angle));
                rix++;
            }
        }

    }

    private void calculateRotationMatrix()
    {
        //total comb(dim,2) rotational planes

        rotationMatrix=new SQMatrix(NDIM);

        for(int x=0;x<rotationAngles.length;x++)
        {
            SQMatrix MP=new SQMatrix(NDIM);
            MP.setVal(rotationAngles[x].p1,rotationAngles[x].p1, (float) Math.cos(rotationAngles[x].angle));
            MP.setVal(rotationAngles[x].p2,rotationAngles[x].p1, -(float) Math.sin(rotationAngles[x].angle));
            MP.setVal(rotationAngles[x].p1,rotationAngles[x].p2, (float) Math.sin(rotationAngles[x].angle));
            MP.setVal(rotationAngles[x].p2,rotationAngles[x].p2, (float) Math.cos(rotationAngles[x].angle));

            rotationMatrix= rotationMatrix.multiplyWith(MP);
        }
        rotationMatrix.printMatrix();
    }
    public void setRandomRotation(){
        createRotationAngles();
        calculateRotationMatrix();
    }

    public void rotate(){

        if(bbusy)
            return;

        for(int i=0;i<(1<<NDIM);i++)
        {
            VEC V=points[i];

            points[i]=rotationMatrix.multiplyWith(V);

        }

    }

    public void printPoints(){


        String str="..\n";
        for (int x=0; x<(1<<NDIM);x++) {


            for (int y = 0; y < NDIM; y++) {

                str=str+ String.format(Locale.getDefault(),"%8.2f",points[x].V[y]);

            }
            str=str+"\n";

        }
        Log.d("AYH",str);

    }


    public void drawbox(Canvas canvas,Paint mp){

        Path path = new Path();

        xoff=canvas.getWidth()/2;
        yoff=canvas.getHeight()/2;
        for(int ei=0;ei<edges.length;ei++)
        {

            int x0=(int)(points[edges[ei].pid1].V[0]*scaleRatio)+xoff;
            int y0=(int)(points[edges[ei].pid1].V[1]*scaleRatio)+yoff;

            int x1=(int)(points[edges[ei].pid2].V[0]*scaleRatio)+xoff;
            int y1=(int)(points[edges[ei].pid2].V[1]*scaleRatio)+yoff;

            path.moveTo(x0,y0 );
            path.lineTo(x1,y1);
        }
        if(selected==2) {
            mp.setStyle(Paint.Style.STROKE);
            mp.setPathEffect(new DashPathEffect(new float[]{5, 10, 15, 20}, 0));
        }
        else
        {
            mp.setStyle(Paint.Style.STROKE);
            mp.setPathEffect(null);
        }
        mp.setColor(color);
        canvas.drawPath(path,mp);
    }

    public  boolean xyinside(Float x, Float y){
        return (x <= (xoff + 100)) && (x >= (xoff - 100)) && (y <= (yoff + 100)) && (y >= (yoff - 100));
    }
}



public class customSurfaceView extends SurfaceView {

    public static Paint mPaint;
    public  static Paint textPaint;

    public static Path path;
    public static Bitmap mBitmap;
    public static Canvas mCanvas;
    private ArrayList<PathWithPaint> _graphics1 = new ArrayList<PathWithPaint>();

    public static int dim=3;

    ArrayList<String> mylist;//Creating arraylist


    //ArrayList<box> myboxes;
    box BOX;

    private GestureDetector mDetector;


    Timer longPressTimer;

    private int createRandomBrightColor() {

        int R=new Random().nextInt(255);
        int G=new Random().nextInt(255);
        int B=new Random().nextInt(255);

        int max;
        if(R>=G && R>=B)
            max=R;
        else if(G>=R && G>=B)
            max=G;
        else
            max=B;

        if(max==0)
        {
            max=1;
        }

        R=(255*R)/max;
        G=(255*G)/max;
        B=(255*B)/max;


        return ((255<<24) | (R<<16) | (G<<8) | (B<<0));
    }




    public customSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        path = new Path();
        mBitmap = Bitmap.createBitmap(820, 480, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);
        this.setBackgroundColor(Color.BLACK);
        mPaint = new Paint();
        mPaint.setDither(true);
        mPaint.setColor(0xFFFFFF00);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(3);


        textPaint=new Paint();
        textPaint.setColor(0xFF808080);
        textPaint.setTextSize(36);

        //myboxes = new ArrayList<box>();

        BOX= new box(3);
        BOX.xoff=this.getWidth()/2;
        BOX.yoff=this.getHeight()/2;
        BOX.color=createRandomBrightColor();

        Timer timer = new Timer();

        final int FPS = 10;
        TimerTask updateBall = new UpdateBallTask();
        timer.scheduleAtFixedRate(updateBall, 0, 1000 / FPS);





    }


    class UpdateBallTask extends TimerTask {
        public void run() {
            //calculate the new position of myBall
            BOX.rotate();
            invalidate();
        }
    };


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if(BOX.bbusy)
            return;
        BOX.drawbox(canvas,mPaint);

        canvas.drawText("DIM:"+String.valueOf(BOX.NDIM) + " CORNERS:"+String.valueOf(BOX.points.length)
                + " EDGES:"+String.valueOf(BOX.edges.length),0,40,textPaint);
    }

    public class PathWithPaint {
        private Path path;
        public Path getPath() {
            return path;
        }
        public void setPath(Path path) {
            this.path = path;
        }
        private Paint mPaint;
        public Paint getmPaint() {
            return mPaint;
        }
        public void setmPaint(Paint mPaint) {
            this.mPaint = mPaint;
        }
    }

    class  countLongPress extends TimerTask {
        int countval;
        Float x;
        Float y;
        public countLongPress(Float ex, Float ey){
            countval=0;
            x=ex;
            y=ey;
        }
        public void run() {
            countval++;
            if(countval>20)
            {

                onLongPress(x,y);
                this.cancel();
            }
        }
    }

    public  void onLongPress(Float x, Float y){
        Log.d("AYH","LONG PRESS DETECTED"+String.valueOf(x)+" "+String.valueOf(y));
    }

    Float xdown,ydown;
    Float xdown1,ydown1;
    boolean canswipe=false;
    boolean canzoom=false;
    int prev_evnt= 9999;

    boolean startZoom=false;
    int change_dim=0;


    @Override
    public boolean onTouchEvent(MotionEvent event) {

        //PathWithPaint pp = new PathWithPaint();
        //mCanvas.drawPath(path, mPaint);
        Float oldx0,oldy0,oldx1,oldy1;

        if(prev_evnt!=(event.getAction()&0xFF)){
            Log.d("AYH","Action:" + String.valueOf((event.getAction()&0xFF)) +  "PTR CNT:" + String.valueOf(event.getPointerCount()) + "\tx="+event.getX()+"\ty="+event.getY() + "index:" + String.valueOf(event.getPointerId(event.getActionIndex())));

            prev_evnt=event.getAction()&0xFF;
        }


        //zoom:
        if ((event.getAction()&0xFF) == MotionEvent.ACTION_POINTER_DOWN)
        {

            if(event.getPointerCount()==2)
            {
                Log.d("AYH","Start Zooming>>>>");
                startZoom=true;
            }
            else if(event.getPointerCount()==1)
            {
                BOX.xoff;
            }

        }
        if ((event.getAction()&0xFF) == MotionEvent.ACTION_UP)
        {

            if(event.getPointerCount()==1)
            {
                if(change_dim!=0)
                {
                    if((BOX.NDIM+change_dim)>1)
                        BOX.reDim(BOX.NDIM+change_dim);

                    change_dim = 0;

                }
                else {
                    BOX.setRandomRotation();
                }
                BOX.color=createRandomBrightColor();
            }
        }
        if(event.getAction()==MotionEvent.ACTION_MOVE)
        {
            if(event.getPointerCount()==2)
            {
               // Log.d("AYH","ZOOM POINT HISTORY:"+ String.valueOf(event.getHistorySize()));

                if(startZoom)
                {
                    if(event.getHistorySize()>0)
                    {
                        oldx0=event.getHistoricalX(0,0);
                        oldx1=event.getHistoricalX(1,0);

                        oldy0=event.getHistoricalY(0,0);
                        oldy1=event.getHistoricalY(1,0);

                        Float newx0=event.getX(0);
                        Float newy0=event.getY(0);
                        Float newx1=event.getX(1);
                        Float newy1=event.getY(1);

                        Float oldDis2= (oldx0-oldx1)*(oldx0-oldx1) + (oldy0-oldy1)*(oldy0-oldy1);

                        Float newDis2=  (newx0-newx1)*(newx0-newx1) + (newy0-newy1)*(newy0-newy1);

                       // Log.d("AYH","O:"+String.valueOf(oldDis2) + " N:"+String.valueOf(newDis2));
                        double zoomRatio= Math.sqrt(newDis2/oldDis2);
                        BOX.zoom(zoomRatio);
                        if(oldDis2>newDis2)
                        {
                            Log.d("AYH","ZOOM OUT <<<<<<");
                        }
                        else if(oldDis2<newDis2)
                        {
                            Log.d("AYH","ZOOM IN >>>>>>>");
                        }

                    }


                }

            }
            else if(event.getPointerCount()==1)
            {
                if(event.getHistorySize()>0)
                {
                    Float oldy = event.getHistoricalY(0,0);
                    Float newy = event.getY(0);
                    Float disty = newy - oldy;

                    Log.d("AYH","disty:"+String.valueOf(disty));

                    if(disty>100)
                    {
                        change_dim=-1;
                    }
                    else if(disty<-100) {
                        change_dim = 1;

                    }


                }
            }
        }



        if(event.getPointerCount()==2)
        {


           // Log.d("AYH","HS P0:"+String.valueOf(event.getHistorySize()) );
            if(true) {

                oldx0=(float)0;
                oldy0=(float)0;
                oldx1=(float)0;
                oldy1=(float)0;

                //oldx0 = (float) event.getHistoricalAxisValue(MotionEvent.AXIS_X, 0, 1);
                //oldy0 = (float) 0;//event.getHistoricalAxisValue(MotionEvent.AXIS_Y,0,1);

                //oldx1 = (float) 0;//event.getHistoricalAxisValue(MotionEvent.AXIS_X,1,1);
                //oldy1 = (float) 0;//event.getHistoricalAxisValue(MotionEvent.AXIS_Y,1,1);
            }
            else
            {
                oldx0=(float)0;
                oldy0=(float)0;
                oldx1=(float)0;
                oldy1=(float)0;
            }

            Float newx0=(float)0;//event.getX(0);
            Float newy0=(float)0;//event.getY(0);
            Float newx1=(float)0;//event.getX(1);
            Float newy1=(float)0;//event.getY(1);


            Float oldDis2= (oldx0-oldx1)*(oldx0-oldx1) + (oldy0-oldy1)*(oldy0-oldy1);

            Float newDis2=  (newx0-newx1)*(newx0-newx1) + (newy0-newy1)*(newy0-newy1);

          //  Log.d("AYH","O:"+String.valueOf(oldDis2) + " N:"+String.valueOf(newDis2));
        }
/*
        if(prev_evnt!=(event.getAction()&0xFF)){
            Log.d("AYH",String.valueOf(event.getAction()&0xFF) + "\tx="+event.getX()+"\ty="+event.getY() + "index:" + String.valueOf(event.getPointerId(event.getActionIndex())));

            prev_evnt=event.getAction()&0xFF;
        }

        Log.d("AYH", String.valueOf(event.getPointerCount()));
        */

        //Log.d("AYH", String.valueOf(event.getPressure(1)));

        return true;
/*
        if (event.getAction() == MotionEvent.ACTION_DOWN) {


            //Log.d("AYH","MotionEvent.ACTION_DOWN" );
            //longPressTimer= new Timer();
            //TimerTask pressTimeCounter = new countLongPress(event.getX(), event.getY());
            //longPressTimer.scheduleAtFixedRate(pressTimeCounter, 0, 1000 / 10);
            if(event.getActionIndex()==0) {
                xdown = event.getX();
                ydown = event.getY();
                canswipe = true;
            }
            else
            {
                canswipe=false;
                canzoom=true;
                xdown = event.getX(0);
                ydown = event.getY(0);

                xdown1 = event.getX(1);
                ydown1 = event.getY(1);


            }

        }
        else if(event.getAction() == MotionEvent.ACTION_UP)
        {
            canswipe=false;

        }

        else if(event.getAction()==MotionEvent.ACTION_MOVE)
        {

            if(canswipe) {
                ydown = ydown + (event.getY() - ydown) / 2;

                if (event.getY() - ydown > 100) {
                    Log.d("AYH", "SWIPE DOWN"+String.valueOf(event.getActionIndex()));


                   // BOX.reDim(BOX.NDIM-1);


                    canswipe = false;
                } else if (event.getY() - ydown < -100) {
                    Log.d("AYH", "SWIPE UP"+String.valueOf(event.getActionIndex()));


                   // BOX.reDim(BOX.NDIM+1);

                    canswipe = false;


                }
            }
            //else if(canzoom)
            {

                xdown = event.getX(0);
                ydown = event.getY(0);

                if(event.getActionIndex()>0) {
                    xdown = event.getX(0);
                    ydown = event.getY(0);

                    xdown1 = event.getX(1);
                    ydown1 = event.getY(1);
                    Log.d("AYH", "x:"+String.valueOf(xdown) + "y:"+String.valueOf(ydown) +
                            "x1:"+String.valueOf(xdown1) + "y1:"+String.valueOf(ydown1));
                }
                else
                {
                    Log.d("AYH", "x:"+String.valueOf(xdown) + "y:"+String.valueOf(ydown) + "EA"+String.valueOf(event.getPointerId(event.getActionIndex())));
                }


            }

        }


        //longPressTimer.cancel();
        //longPressTimer.purge();

        return true;
*/

    }




}