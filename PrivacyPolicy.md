Welcome to privacy policy document for app x-cube
This is an open source Android app developed by Ayhan Yuksel. The source code is available on Gitlab; the app is also available on Google Play.

I have not programmed this app to collect any personally identifiable information. All data (such as app preferences ) created by the you is stored on your device only, and can be simply erased by clearing the app's data or uninstalling it.
If you find any security vulnerability that has been inadvertently caused by me, or have any question regarding how the app protectes your privacy, please send me an email and I will surely try to fix it/help you.
Yours sincerely,

Ayhan.
ayhanyks82@gmail.com
